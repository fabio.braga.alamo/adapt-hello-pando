define([
    "core/js/adapt",
    "core/js/views/componentView",
    "core/js/models/componentModel"
], function(Adapt, ComponentView, ComponentModel) {

   var helloPandoView = ComponentView.extend({
      events: {
         'click .connect-from .box .dot': 'onClickFromItem',
         'click .connect-to .box .dot': 'onClickToItem'
      },

      postRender: function() {
         this.randomize('.connect-from');
         this.randomize('.connect-to');
      },

      onClickFromItem: function(e) {
         this.$(".connect-from .dot").removeClass("active");
         this.$(".connect-to").addClass("active");
         this.$(e.currentTarget).addClass("active");
      },

      onClickToItem: function(e) {

         this.$(".connect-to").removeClass("active");

         var from = this.$(".connect-from .dot.active").parents(".box");
         var to = this.$(e.currentTarget).parents(".box");

         if (from.data("value") == to.data("value")) {

            this.$(".connect-from .dot.active").addClass("correct");
            this.$(e.currentTarget).addClass("correct");
            this.draw_line(this.$(".connect-from .dot.active"), this.$(e.currentTarget));

         } else {

            from.find(".dot").addClass("wrong");
            to.find(".dot").addClass("wrong");

            this.draw_line(this.$(from).find(".dot"), this.$(to).find(".dot"), true);

            setTimeout(function () {
               this.$(".connect-from .dot").removeClass("wrong active");
               this.$(".connect-to .dot").removeClass("wrong active");
            }, 800);

         }

         if (this.$('.connect-from .dot').not(".correct").length == 0) {
            this.$(window).trigger("finish");
            let event = new Event("finish", { finish: true }); // (2)
            window.dispatchEvent(event);
         }

      },

      randomize(item) {
         var list = this.$(item);
         // pega os itens da lista
         var array = list.children('.box');
         // sort array of list items in current ul randomly
         array.sort(function (a, b) {
            var temp = parseInt(Math.random() * array.length);
            var isOddOrEven = temp % 2;
            var isPosOrNeg = temp > 5 ? 1 : -1;
            return (isOddOrEven * isPosOrNeg);
         })
            // adiciona os itens na lista
            .appendTo(list);
      },

      draw_line(from, to, wrong) {

         div1 = this.$(from);
         div2 = this.$(to);

         var line = $("<line/>");

         var pos1 = div1.offset();
         var pos2 = div2.offset();
         var middle_left = this.$('.middle').offset().left;
         var middle_top = this.$('.middle').offset().top;
         var diference = (div1.width() / 2) - 1;

         line
            .attr('x1', pos1.left - middle_left + diference)
            .attr('y1', pos1.top - middle_top + diference)
            .attr('x2', pos2.left - middle_left + diference)
            .attr('y2', pos2.top - middle_top + diference);

         if (wrong) {

            $(line).addClass("wrong");

            setTimeout(function () {
               this.$(".middle svg line:last").remove();
            }, 800);

         }

         this.$(".middle svg").append(line);
         this.$(".middle").html(this.$(".middle").html());
      }
   });

   //  var helloPandoModel = ComponentModel.extend({
       
   //  });

    return Adapt.register("hello-pando", {
       view: helloPandoView,
    });
   
});